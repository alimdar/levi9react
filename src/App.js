import React, {Component} from 'react';
import './app/styles/App.css';
import { withRouter} from 'react-router-dom';
import {connect} from 'react-redux';

import Header from './app/header/header';
import Modal from './app/modal/modal';
import Content from './app/content/content';
import {getModal} from './app/redux_main/selectors';

class App extends Component {

  render() {
    return (
        <div className="App">
          <Header/>
          <Content/>
          {this.props.modal ? <Modal/> : null}
        </div>
    );
  }
}

const mapStateToProps = (state) => ({
  modal: getModal(state)
});

export default withRouter(connect(mapStateToProps)(App));
