import * as _ from "lodash";

export function formatResponseForThumbnails(items) {
  if(!items.length){
    return []
  }

  if(items[0].snippet){
    return  _.map(items, (item) => {
      return  {
        id: item.id.videoId,
        author: item.snippet.channelTitle,
        title: item.snippet.title,
        backgroundImage: item.snippet.thumbnails.medium.url,
        type: 'videos'
      }
    });

  } else {
    return  _.map(items, (item) => {
      return  {
        id: item.id,
        author: item.owner,
        title: item.title,
        backgroundImage: `https://farm${item.farm}.static.flickr.com/${item.server}/${item.id}_${item.secret}_m.jpg`,
        fullImage: `https://farm${item.farm}.staticflickr.com/${item.server}/${item.id}_${item.secret}_z.jpg`,
        type: 'images'
      }
    });
  }
}

export function parseFlickrJson(flickrUrl){
    return fetch(flickrUrl).then((resp) => {
        return resp.text().then((text) => {
            let jsonString = text.replace('jsonFlickrApi(', '');
            jsonString = jsonString.substring(0, jsonString.length - 1);
            return JSON.parse(jsonString);
        })
    });
}

export function formatPhoto(obj){
    return {
        title: obj.title._content,
        author: obj.owner.realname,
        url: obj.urls.url[0]._content,
        type: 'images'
    }
}