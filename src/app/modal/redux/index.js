import * as modalActions from './ducks/modal.actions';
import * as modalSelectors from './ducks/modal.selectors';
import modalReducer from './ducks/modal.reducer';

export {
  modalActions,
  modalSelectors
}

export default modalReducer