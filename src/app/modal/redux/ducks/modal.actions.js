import * as constants from './modal.constants';

export const openModal = (modalSettings) => {
  return {
    type: constants.OPEN_MODAL,
    payload: modalSettings
  }
};

export const closeModal = () => {
  return {
    type: constants.CLOSE_MODAL
  }
};