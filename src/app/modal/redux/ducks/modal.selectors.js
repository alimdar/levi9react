// selectors
export const getModal = (state) => state.modal;
export const getModalSettings = (state) => state.modalSettings ? state.modalSettings : null;