import * as constants from './modal.constants';

export default function(state = {}, action){

  switch(action.type){

    case constants.OPEN_MODAL:

      return {
        ...state,
        modal:true,
        modalSettings: action.payload};

    case constants.CLOSE_MODAL:

      return {...state, modal:false, modalSettings: ''};

    default:
      return state;
  }
}