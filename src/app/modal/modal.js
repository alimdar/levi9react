import React, {Component} from 'react';
import {connect} from 'react-redux';
import {modalActions} from '../redux_main/actions';
import {getModal, getModalSettings} from '../redux_main/selectors';


// import {closeModal} from './ducks/modal.actions';
import {withRouter} from 'react-router';
import {Icon} from 'semantic-ui-react'
import './modal.css'
import './../styles/general.css'

class Modal extends Component {

    goToSearch = (platform) => {

        this.props.closeModal();
        if(this.props.settings.uploadFiles){
          this.props.history.push(`/search/add/${platform}`);
        } else {
          this.props.history.push(`/search/${platform}`);
        }
    };

    render() {

        return (

            <div className="modal-backdrop">
                <div className="modal-wrapper">
                    <div className="modal-header">
                        <div className="modal-title">{this.props.settings.title}</div>
                        <div className="close" onClick={this.props.closeModal.bind(this)}>
                            <Icon name="remove"> </Icon>
                        </div>
                    </div>

                    <div className="modal-body">
                        {this.props.settings.template ?
                            this.props.settings.template :

                            <React.Fragment>
                              { this.props.settings.uploadFiles ? null :
                                <div className="pick-item myfiles"
                                     onClick={this.goToSearch.bind(this, 'mediasaved')}>
                                </div>
                              }
                                <div className="pick-item videos"
                                     onClick={this.goToSearch.bind(this, 'videos')}> </div>
                                <div className="pick-item images"
                                     onClick={this.goToSearch.bind(this, 'images')}> </div>
                                <div className="clear-fix"> </div>
                            </React.Fragment>
                        }

                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    modal: getModal(state),
    settings: getModalSettings(state)
});

const mapActionsToProps = {
  ...modalActions
};

export default withRouter(connect(mapStateToProps, mapActionsToProps)(Modal));
