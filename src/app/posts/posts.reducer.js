import * as constants from './posts.constants';
// import {setPostToLocalStorage, getItemFromLocalStorage} from '../localstorage/localstorage-service';
import * as _ from 'lodash';

export default function (state = {
                           myPosts: [],
                           cachedValues: {},
                            singlePost: {}
                         },
                         action) {

  switch (action.type) {

    case constants.GET_SINGLE_POST:
      return {
        ...state,
        singlePost: getPost(state, action.payload)
      };

    case constants.CACHE_VALUES:
      return {
        ...state,
        cachedValues: action.payload
      };

    case constants.SAVE_POST:
      return {
        ...state,
        myPosts: [...state.myPosts, action.payload]
      };
    case constants.DELETE_POST:
      return {
        ...state,
        myPosts: filterPosts(state, action.payload)
      };

    default:
      return state;
  }
}

function getPost(state, id){
  return state.myPosts.filter((post) => _.toString(post.id) === _.toString(id))[0];
}

function filterPosts(state, id) {
  return state.myPosts.filter((post)=> _.toString(post.id) !== _.toString(id))
}

// Selectors

export const _getCachedValues = (state) => state.cachedValues;
export const _singlePost = (state) => state.singlePost || {};
export const _getMyPosts = (state) => state.myPosts;