import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Link, withRouter} from 'react-router-dom';
import '../../styles/general.css'
import './posts-list.css'
import MediaThumb from '../media/media-thumb/media-thumb';
import {Icon} from 'semantic-ui-react';
import InputSearch from "../media/input-search/input-search";
import PropTypes from 'prop-types';
import {getMyPosts} from '../../redux_main/selectors';

class PostsList extends Component {

  state = {
    searchTermFilter: '',
    searchWasMade: false
  };

  goToPost = (thumb) => {
    this.props.history.push(`/post/${thumb.id}`);
  };

  filterSearchPosts = (event) => {
    if (event.keyCode === 13 && this.props.myPosts.length) {
      this.setState({searchTermFilter: event.target.value, searchWasMade: true});
    }
  };

  goToNewPost = () => {
    this.props.history.push('/newpost');
  };

  render() {

    let postThumbs = this.props.myPosts.filter((thumb) => {
      return thumb.title.toLowerCase().indexOf(this.state.searchTermFilter.toLocaleLowerCase()) !== -1;
    });

    return (
      <div className="page posts-list">
        <div className="title">My posts</div>
        <InputSearch
          onAdd={this.goToNewPost}
          onSearch={this.filterSearchPosts}
          placeholder={'Search your saved files'}/>

        {postThumbs.length ?
          <React.Fragment>
            {postThumbs.map((thumb) => (
              <MediaThumb
                key={thumb.id}
                thumb={thumb}
                onSelectThumb={this.goToPost}
                type='post'/>
            ))}
          </React.Fragment> :

          <React.Fragment>
            {this.state.searchWasMade ?
                <div className="empty-state">
                  <h1><b>No search results. Try searching for something else</b></h1>
                </div> :

                <div className="empty-state">
                  <h1><b>There is nothing here.</b></h1> <br/>
                  add some posts by clicking the &nbsp;
                  <Link to="/newpost"><Icon name="add" circular/>&nbsp;</Link>button.
                </div>
            }
          </React.Fragment>
        }
      </div>
    )
  }

  static propTypes = {
    myPosts: PropTypes.array
  }
}

const mapStateToProps = (state) => ({
  myPosts: getMyPosts(state)
});

export default withRouter(connect(mapStateToProps)(PostsList));
