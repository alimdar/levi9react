import React,  {Component} from 'react';
import './post-page.css';
import './../../styles/general.css';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {getSinglePost} from './../posts.actions';
import MediaPreview from "../media/media-preview/media-preview";
import PropTypes from 'prop-types';
import {singlePost} from "../../redux_main/selectors";

class PostPage extends Component {

  componentDidMount(){
    this.props.getSinglePost(this.props.match.params.postId);
  }

  render(){
    let singlePostData = this.props.singlePost;
    return (
      <div className="page post-page">
          {this.props.singlePost ?
              <React.Fragment>
                  <MediaPreview post={singlePostData.includedMedia}/>
                  <div className="data">{singlePostData.title} </div>
              </React.Fragment> :
              <div>There is nothing here</div>
          }
      </div>
    )
  }

  static propTypes = {
    singlePost: PropTypes.object
  }
}

const mapStateToProps = (state) => ({
    singlePost: singlePost(state)
});

export default withRouter(connect(mapStateToProps, {getSinglePost})(PostPage));
