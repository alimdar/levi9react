import * as constants from './media.constants';
// import * as localStorageService from '../../localstorage/localstorage-service';
import {getPostsYouTube, searchPostsFlickr, searchPostsYouTube, getPostsFlickr} from './media-service';
import {formatPhoto} from '../../utils/utils';

export const getSingleMedia = (mediaId, type) => {
  return function(dispatch){
    if(type === 'videos'){
      getPostsYouTube(mediaId).then((result) => {
        dispatch({
          type: constants.GET_SINGLE_MEDIA,
          payload: result.items
        })
      });
    } else {
      getPostsFlickr(mediaId).then((result) => {
        dispatch({
          type: constants.GET_SINGLE_MEDIA,
          payload: formatPhoto(result.photo)
        })
      })
    }
  }
};

export const addMediaToMyList = (post) => {
    post.isSaved = true;
    return {
        type: constants.ADD_MEDIA_TO_MY_LIST,
        payload:post
    };
};

export const removeMediaFromList = (post) => {
    post.isSaved = false;
    return {
        type: constants.REMOVE_MEDIA_FROM_MY_LIST,
        payload: post
    }
};

export const selectedPostMedia = (selectedMedia) => {
  return {
    type: constants.MEDIA_FOR_POST,
    payload: selectedMedia
  }
};

export const clearPostMedia = () => {
  return {
    type: constants.CLEAR_POST_MEDIA
  }
};

export const searchMedia = (term, platform ) => {
    return function(dispatch){
        if(platform === 'videos'){
          searchPostsYouTube(term).then((result) => {
            dispatch(
              {
                type: constants.SEARCH_MEDIA,
                payload: result.items
              }
            );
          })

        } else if(platform === 'images'){
            searchPostsFlickr(term).then((result) => {
                dispatch(
                    {
                        type: constants.SEARCH_MEDIA,
                        payload: result.photos.photo
                    }
                );
            })
        }
    }
};

export const clearSearchResults = () => {
    return {
        type: constants.CLEAR_SEARCH_MEDIA
    }
};