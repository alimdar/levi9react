import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {Icon} from 'semantic-ui-react';

//style
import '../../../styles/general.css';
import './media-saved.css';

//components
import MediaThumb from '../media-thumb/media-thumb';

// actions
import {openModal} from '../../../modal/redux/ducks/modal.actions';
import InputSearch from '../input-search/input-search';
import {getSavedMedia} from '../../../redux_main/selectors';
import PropTypes from "prop-types";


class MediaSaved extends Component {

    state = {
      searchTerm: '',
      searchTermFilter: '',
      searchWasMade: false
    };

    openModalToAddFiles = () => {
      let modalSettings = {
        title: 'Search for media',
        uploadFiles: true
      };
      this.props.openModal(modalSettings);
    };

    searchForMediaOnSubmit = (event) => {
        if (event.keyCode === 13 && this.props.savedMedia.length) {
            this.setState({searchTermFilter: event.target.value});
            this.setState({searchWasMade: true});
        }
    };

    render() {
        let mediaThumbs = this.props.savedMedia.filter((thumb) => {
          return thumb.title.toLowerCase().indexOf(this.state.searchTermFilter.toLocaleLowerCase()) !== -1;
        });

        return (
            <div className="page posts-list media-list">
              <div className="title">My saved media</div>
                <InputSearch
                    onAdd={this.openModalToAddFiles}
                    onSearch={this.searchForMediaOnSubmit}
                    placeholder={'Search your saved files'}/>

                {mediaThumbs.length > 0 ?
                    <React.Fragment>
                        {mediaThumbs.map((thumb) => (
                            <MediaThumb
                                onSelectThumb={() => {}}
                                thumb={thumb}
                                context={'list'}
                                key={thumb.id}>
                            </MediaThumb>
                        ))}
                    </React.Fragment> :

                    <React.Fragment>
                      { this.state.searchWasMade ?
                          <div className="empty-state">
                            <h2><b>No results. Try searching for something else</b></h2>
                          </div> :
                          <div className="empty-state">
                            <h2><b>You have no saved files.</b></h2>Search for some files here<br/><br/>
                            <Icon name="search" circular onClick={this.openModalToAddFiles}/>&nbsp;
                          </div>
                      }
                    </React.Fragment>
                }
            </div>
        )
    }

  static propTypes = {
    savedMedia: PropTypes.array,
    openModal: PropTypes.func.isRequired,
  };
}

const mapStateToProps = (state) => ({
    savedMedia: getSavedMedia(state)
});

export default withRouter(connect(mapStateToProps, {openModal})(MediaSaved));