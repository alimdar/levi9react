import React, {Component} from 'react';
import {Icon, Input} from 'semantic-ui-react';
import './input-search.css';
import PropTypes from 'prop-types';

class InputSearch extends Component {

    state = {
        searchTerm:''
    };

    setValue = (event) => {
        this.setState({searchTerm: event.target.value});
    };

    render() {
        return (
            <div className='input-search-holder'>
                <Input className='input-search'
                       onChange={this.setValue}
                       value={this.searchTerm}
                       onKeyDown={this.props.onSearch}
                       placeholder={this.props.placeholder || 'Search'} />
                <Icon name="add" circular onClick={this.props.onAdd}/>
            </div>
        )
    }

  static propTypes = {
    onSearch: PropTypes.func.isRequired,
    placeholder: PropTypes.string,
    onAdd: PropTypes.func.isRequired,
    searchTerm: PropTypes.string
  };
}

export default InputSearch;