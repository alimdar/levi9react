import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {Input, Button, Icon} from 'semantic-ui-react'
import * as _ from 'lodash';
import PropTypes from 'prop-types';

// style
import './media-search.css'
import '../../../styles/general.css'

// components
import MediaThumb from "../media-thumb/media-thumb";
import MediaPreview from "../media-preview/media-preview";

// actions
import {searchMedia, clearSearchResults, selectedPostMedia} from './../media.actions';
import {getSavedMedia, searchedMedia} from "../../../redux_main/selectors";

class MediaSearch extends Component {

  constructor(props) {
    super(props);
    this.state = {
      searchTerm: '',
      selectedMedia: null,
      searchTermFilter: ''
    };
  }

    // clear search result
  componentWillUnmount() {
    this.props.clearSearchResults();
  };

  // clear search result
  componentDidUpdate(prevProps) {
    if (this.props.match.params.platform !== prevProps.match.params.platform) {
      this.props.clearSearchResults();
      this.setState({searchTerm: '', selectedMedia: null});
    }
  };

  // set search term
  setTerm = (event) => {
    this.setState({searchTerm: event.target.value});
  };

  // send search action on enter
  searchForMediaOnSubmit = (event) => {
    if (event.keyCode === 13) {
      if (this.props.match.params.platform === 'mediasaved') {
        this.setState({searchTermFilter: event.target.value});
      } else {
        this.props.searchMedia(event.target.value, this.props.match.params.platform);
      }
      this.setState({selectedMedia: null})
    }
  };

  // select thumb
  selectThumb = (thumb) => {
    this.setState({selectedMedia: thumb});
  };

  savePost = () => {
    this.props.selectedPostMedia(this.state.selectedMedia);
    this.goBack();
  };

  checkIfSavedToMyFiles = (searchedMedia, savedMedia) => {
    let savedIds = _.map(savedMedia, (file) => {
      return file.id;
    });
    _.forEach(searchedMedia, (media) => {
        media.isSaved = savedIds.indexOf(media.id) !== -1;
    });
    return searchedMedia;
  };

  filterOnSearch = (savedMedia) => {
    return _.filter(savedMedia, (media) => {
        return media.title.toLowerCase().indexOf(this.state.searchTermFilter.toLocaleLowerCase()) !== -1;
    })
  };

  goBack = () => {
    this.props.history.goBack();
  };

  render() {

    const savedMedia = this.props.match.params.platform === 'mediasaved';
    const mediaType = (this.props.match.params.platform === 'videos') ? 'videos' : (this.props.match.params.platform === 'images') ? 'images' : 'my files';
    let mediaResults = savedMedia ? this.filterOnSearch(this.props.savedMedia) : this.checkIfSavedToMyFiles(this.props.searchedMedia, this.props.savedMedia);

    return (
      <div className="page post-search">
        <div className="segment left">
          <div className={`logo ${this.props.match.params.platform}`}> </div>

          <Button className="back-button"
                  icon labelPosition="left"
                  onClick={this.goBack}>
            <Icon name="left arrow"/>Go back
          </Button>

          <div className="search">
            <Input placeholder={`Search for ${mediaType}`}
                   value={this.state.searchTerm}
                   onChange={this.setTerm}
                   onKeyDown={this.searchForMediaOnSubmit}/>
          </div>

          <div className="search-title">
            {this.state.searchTerm ?
              <span>Results for <i>{this.state.searchTerm}</i>: <b>{mediaResults.length}</b></span> :
              null
            }
          </div>

          <div className="items-list">
            {mediaResults.map((thumb) => (
              <MediaThumb context={'search'}
                          platform={mediaType}
                          isSelected={this.state.selectedMedia && this.state.selectedMedia.id === thumb.id}
                          thumb={thumb}
                          onSelectThumb={this.selectThumb}
                          key={thumb.id}/>
            ))}
          </div>
        </div>

        <div className="segment right">

          {this.state.selectedMedia ?
            <React.Fragment>
              <MediaPreview post={this.state.selectedMedia}/><br/><br/>
              {this.props.location.pathname === '/search/images' || this.props.location.pathname === '/search/videos' || this.props.location.pathname === '/search/mediasaved'?
                  <Button compact size='small' inverted color='blue' onClick={this.savePost}>
                    <Icon name="add"> </Icon>Include in post
                  </Button> : null
              }

            </React.Fragment>
            :
            <div className="empty-panel">
              <h1><Icon name='mouse pointer' circular/></h1>
              <br/><br/>Select a file to view post preview.
            </div>
          }
        </div>
      </div>
    )
  }
  static propTypes = {
    searchedMedia:PropTypes.array,
    savedMedia:PropTypes.array,
    searchMedia:PropTypes.func,
    clearSearchResults:PropTypes.func,
    selectedPostMedia:PropTypes.func,
  };
}

function mapStateToProps(state) {
  return {
    searchedMedia: searchedMedia(state),
    savedMedia: getSavedMedia(state)
  }
}
export default withRouter(connect(mapStateToProps, {searchMedia, clearSearchResults, selectedPostMedia})(MediaSearch));
