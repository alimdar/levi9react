import React, {Component} from 'react';
import {connect} from 'react-redux';

import '../../../styles/general.css';
import './media-thumb.css';
import { Icon } from 'semantic-ui-react'
import MediaActionButton from './../media-action-button/media-action-button';
import {addMediaToMyList, removeMediaFromList} from "./../media.actions";
import PropTypes from "prop-types";

class MediaThumb extends Component {

    selectThumb = () => {
        this.props.onSelectThumb(this.props.thumb);
    };

    render() {
        const thumb = this.props.thumb;
        return (
            <div className={`post-thumb ${this.props.context}` + (this.props.isSelected ? ' selected' : '')}
                 id={thumb.id}
                 onClick={this.selectThumb}>


              { !thumb.backgroundImage ?
                  <div className="thumb-image placeholder"><Icon name="image"/></div> :
                  <div className="thumb-image"
                       style={{backgroundImage: `url(${thumb.backgroundImage})`}}> </div>
              }

                <div className="thumb-data">
                    <div className={"name " + (this.props.isSelected ? 'bold' : '')}>{thumb.title}</div>
                    <div className="author">{thumb.author}</div>
                </div>
              {/*{this.props.type === 'post' ? null : <MediaActionButton file={this.props.thumb}/>}*/}
              {this.props.type === 'preview' ? null : <MediaActionButton type={this.props.type} file={this.props.thumb}/>}
            </div>
        )
    }

  static propTypes = {
    thumb: PropTypes.object.isRequired,
    onSelectThumb: PropTypes.func,
    context:PropTypes.oneOf(['list', 'search']),
    isSelected: PropTypes.bool,
  };
}

export default connect(null, {addMediaToMyList, removeMediaFromList})(MediaThumb);
