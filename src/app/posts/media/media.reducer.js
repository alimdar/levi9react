import * as constants from './media.constants';
import * as _ from 'lodash';
import {formatResponseForThumbnails} from './../../utils/utils';

export default function (state = {
  savedMedia: [],
  singleMedia: {},
  searchedMedia: [],
  mediaForPost: ''
}, action) {

  switch (action.type) {

    case constants.GET_SAVED_MEDIA:
      return {
        ...state,
        savedMedia: action.payload ? action.payload : []
      };

    case constants.GET_SINGLE_MEDIA:
      return {
        ...state,
        singleMedia: action.payload
      };

    case constants.MEDIA_FOR_POST:
      return {
        ...state,
        mediaForPost: action.payload
      };

    case constants.ADD_MEDIA_TO_MY_LIST:
      return {
        ...state,
        savedMedia: [...state.savedMedia, action.payload]
      };

    case constants.REMOVE_MEDIA_FROM_MY_LIST:
      return {
        ...state,
        savedMedia: removePostFromList(state.savedMedia, action.payload.id)
      };

    case constants.SEARCH_MEDIA:
      return {
        ...state,
        searchedMedia: formatResponseForThumbnails(action.payload)
      };

    case constants.CLEAR_POST_MEDIA:
      return {
        ...state,
        mediaForPost: null
      };

    case constants.CLEAR_SEARCH_MEDIA:
      return {
        ...state,
        searchedMedia: []
      };

    default:
      return state;
  }
}

function removePostFromList(savedItems, id) {
  return  _.filter(savedItems, (item) => item.id !== id);
}

// Selectors
export const _getSavedMedia = (state) => state.savedMedia;
export const _searchedMedia = (state) => state.searchedMedia;
export const _getMediaForPost = (state) => state.mediaForPost;