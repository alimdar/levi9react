import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Button, Icon} from 'semantic-ui-react';
import PropTypes from 'prop-types';
import './media-action-button.css';
import {addMediaToMyList, removeMediaFromList} from './../media.actions';
import {deletePost} from './../../posts.actions';

class MediaActionButton extends Component {

    thumbAction = (e) => {
        e.stopPropagation();

        if (this.props.file.isSaved) {
            this.props.removeMediaFromList(this.props.file, this.props.file.type);
        } else {
            this.props.addMediaToMyList(this.props.file, this.props.file.type);
        }
      this.forceUpdate();
    };

    removeItemFromList = (e) => {
      e.stopPropagation();
      this.props.deletePost(this.props.file.id);
    };

    render() {
      if(this.props.type === 'post'){
        return (
          <div className="remove-post" onClick={this.removeItemFromList}>
            <Icon name="remove circle"/>
          </div>
        )
      } else {
        return (
          <React.Fragment>
            {this.props.file.isSaved ?
              <Button compact size='small' inverted color='red' onClick={this.thumbAction}>
                <Icon name="remove"> </Icon>Remove from my files</Button> :

              <Button compact size='small' inverted color='blue' onClick={this.thumbAction}>
                <Icon name="add"> </Icon>Add to my files</Button>
            }
          </React.Fragment>
        )
      }
    }

  static propTypes = {
    file: PropTypes.object.isRequired,
    type: PropTypes.oneOf(['images', 'videos', 'post']),
    isSaved: PropTypes.bool,
    removeMediaFromList: PropTypes.func.isRequired,
    addMediaToMyList: PropTypes.func.isRequired
  };

}
export default connect(null, {addMediaToMyList, removeMediaFromList, deletePost})(MediaActionButton);
