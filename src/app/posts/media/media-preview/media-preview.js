import React from 'react';
import './media-preview.css';
import PropTypes from 'prop-types';

const MediaPreview = (props) => {
  if (!props.post) {
    return <div>loading...</div>;
  }
  return (
    <div className="preview-post">
      <div className="post-frame">

        {props.post.type === 'videos' ?

          <iframe title="video" width="100%"
                  height="100%"
                  src={`https://www.youtube.com/embed/${props.post.id}`}></iframe> :

          <img alt="img" className='preview-image' src={props.post.fullImage}/>

        }
      </div>

      <div className="post-data">
        <div className="name">{props.post.title}</div>
        <div className="author">{props.post.author}</div>
      </div>
    </div>
  )
};

MediaPreview.propTypes = {
  post: PropTypes.object
};
export default MediaPreview;