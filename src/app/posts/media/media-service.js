import * as constants from "./media.constants";
import {parseFlickrJson} from '../../utils/utils'

// SEARCH YOUTUBE
export function searchPostsYouTube(term) {
  const youTubeUrl = `https://www.googleapis.com/youtube/v3/search?q=${term}=date&part=snippet&type=video&maxResults=50&key=${constants.YOUTUBE_API_KEY}`;
  return fetch(youTubeUrl)
    .then(response => response.json()
      .then(responseJson => {
        return responseJson
      })
    )
}

// GET POSTS PER ID YOUTUBE
export function getPostsYouTube(videoIds) {
  const youTubeUrl = `https://www.googleapis.com/youtube/v3/videos?key=${constants.YOUTUBE_API_KEY}&part=snippet&id=${videoIds}`;
  return fetch(youTubeUrl)
    .then(response => response.json()
      .then(responseJson => {
        return responseJson
      })
    )
}

// SEARCH FLICKR
export function searchPostsFlickr(term) {
  const flickrUrl = `https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=${constants.FLICKR_API_KEY}&format=json&text=${term}&media=photos`;
  return parseFlickrJson(flickrUrl);
}

export function getPostsFlickr(id) {
  const flickrUrl = `https://api.flickr.com/services/rest/?method=flickr.photos.getInfo&api_key=${constants.FLICKR_API_KEY}&photo_id=${id}&format=json&media=photos`;
  return parseFlickrJson(flickrUrl);
}