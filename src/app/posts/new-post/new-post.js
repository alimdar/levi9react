import React, {Component} from 'react';
import {connect} from 'react-redux';
import '../../styles/general.css';
import './new-post.css';
import {openModal} from '../../modal/redux/ducks/modal.actions';
import {savePost, cacheFormValues} from '../posts.actions';
import {getUser, getMediaForPost, getCachedValues, getFormValues} from '../../redux_main/selectors';

import { withRouter } from 'react-router-dom';
import {clearPostMedia} from '../media/media.actions';
import NewPostForm from './new-post-form';

class NewPost extends Component {

  onSubmitPost = () => {

    this.props.cacheFormValues(null);
    let formValues = {
      ...this.props.formValues,
      id: new Date().getTime(),
      author: this.props.user.name,
      backgroundImage: this.props.mediaForPost ? this.props.mediaForPost.backgroundImage : '',
      includedMedia: this.props.mediaForPost
    };
    this.props.savePost(formValues);
    this.props.clearPostMedia();
    this.props.history.push('/');
  };

  render() {
    return (
      <div className="page">
        <div className="new-post-container">
          <NewPostForm initialValues={this.props.cachedValues} onSubmit={this.onSubmitPost}/>
        </div>
      </div>
    )
  }
}


function mapStateToProps(state) {
  return {
    cachedValues: getCachedValues(state),
    user: getUser(state),
    mediaForPost: getMediaForPost(state),
    formValues: getFormValues(state, 'newPost')
  }
}

const actions = {openModal, savePost, cacheFormValues, clearPostMedia};

export default withRouter(connect(mapStateToProps, actions)(NewPost));
