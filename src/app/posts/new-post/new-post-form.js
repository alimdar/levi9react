import React, {Component, Fragment} from 'react';
import {Field, Form, reduxForm} from 'redux-form';
import {Button, Icon, Input} from 'semantic-ui-react';
import {getFormValues, getMediaForPost} from "../../redux_main/selectors";
import {connect} from "react-redux";
import MediaThumb from '../media/media-thumb/media-thumb';
import {cacheFormValues} from '../posts.actions';
import {openModal} from '../../modal/redux/ducks/modal.actions';

class NewPostForm extends Component {

  openModalToAddMedia = (e) => {
    e.preventDefault();
    this.props.cacheFormValues(this.props.formValues);
    this.props.openModal({title: 'Search for media'});
  };

  renderInput = (field) => (
    <Fragment>
      <label>{field.label}</label>
      <Input {...field.input}
             id={field.name}
             type={field.type}
             placeholder={field.placeholder}/>
      {
        field.meta.error && field.meta.touched &&
        <div className="error-message">
          {field.meta.error}
        </div>
      }
    </Fragment>
  );


  renderTextArea = (field) => (
    <Fragment>
      <label>{field.label}</label>
      <textarea className="textarea"
                {...field.input}
                id={field.name}
                placeholder={field.placeholder}/>
      {
        field.meta.error && field.meta.touched &&
        <div className="error-message">
          {field.meta.error}
        </div>
      }
    </Fragment>
  );

  render() {

    const {handleSubmit} = this.props;

    return (
      <Form onSubmit={handleSubmit}>
        <Field name="title"
               type="text"
               label="Title"
               component={this.renderInput}
               placeholder="Add title for post"/>
        <Button className="margin10 add-media"
                icon labelPosition="left"
                onClick={this.openModalToAddMedia}>
          Add media
          <Icon name="add"/>
        </Button>
        <div className="added-media">
          {this.props.mediaForPost ?
            <MediaThumb
              key={this.props.mediaForPost.id}
              thumb={this.props.mediaForPost}
              type={'preview'}
            /> : null}
        </div>
        <Field name="content"
               label="Content for your post"
               component={this.renderTextArea}
               placeholder="Add content for your post"/>
        <Button basic color='blue'>Save post</Button>
      </Form>
    )
  }
}

const validate = values => {
  const errors = {};
  if (!values.title) {
    errors.title = 'Enter a title';
  }

  if (!values.content) {
    errors.content = 'Enter a content';
  }

  return errors;
};

NewPostForm = reduxForm({
  validate,
  form: 'newPost'
})(NewPostForm);

function mapStateToProps(state) {
  return {
    mediaForPost: getMediaForPost(state),
    formValues: getFormValues(state, 'newPost')
  }
}

export default connect(mapStateToProps, {cacheFormValues, openModal})(NewPostForm);
