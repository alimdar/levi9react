export const GET_POSTS = 'GET_POSTS';
export const GET_SINGLE_POST = 'GET_SINGLE_POST';
export const SAVE_POST = 'SAVE_POST';
export const CACHE_VALUES = 'CACHE_VALUES';
export const DELETE_POST = 'DELETE_POST';

