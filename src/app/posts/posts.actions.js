import * as constants from './posts.constants';

export const deletePost = (postId) => {
  return {
    type: constants.DELETE_POST,
    payload: postId
  }
};

export const getSinglePost = (postId) => {
  return function (dispatch) {
    // single post
    dispatch(
      {
        type: constants.GET_SINGLE_POST,
        payload: postId
      }
    );
  }
};

export const savePost = (post) => {
  return {
    type: constants.SAVE_POST,
    payload: post
  }
};

export const cacheFormValues = (values) => {
  return {
    type: constants.CACHE_VALUES,
    payload: values
  }
};