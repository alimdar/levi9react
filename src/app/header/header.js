import React, {Component} from 'react';
import './header.css';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {Icon, Popup} from 'semantic-ui-react';
import {signOut} from '../authenticate/authentication.actions';

class Header extends Component {

  signOut = () => {
    this.props.signOut();
  };

  render() {

    return (
      <div className="react-header">
        <Link className="home-link" to="/">React Levi9</Link>
        {this.props.authenticated ?
          <div>
            <div className="info header-icon">{this.props.user.name}</div>
              <Popup trigger={<Link className="my-files header-icon" to="/mediasaved"><Icon name="folder open outline"/></Link>} content="My saved media"/>
              <Popup trigger={<div className="sign-out header-icon" onClick={this.signOut}><Icon name="sign out"/></div>} content="Sign out"/>
              <Popup trigger={<Link className="add-new header-icon" to="/newpost"><Icon name="add"/></Link>} content="Create new post"/>
          </div> :
          null
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
    authenticated: state.auth.authenticated,
    user: state.auth ? state.auth.user : null
});

export default connect(mapStateToProps, {signOut})(Header);
