import React, {Component} from 'react';
import { Segment } from 'semantic-ui-react';
import {connect} from 'react-redux';
import { GoogleLogin } from 'react-google-login';
import {signInSuccess} from "../authentication.actions";
import {isAuthenticated} from "../../redux_main/selectors";

import './login.css';
import '../../styles/general.css'
import {CLIENT_ID} from "../autentication.constants";

class Login extends Component {

  constructor(props) {
    super(props);
    this.responseGoogle = this.responseGoogle.bind(this);
  }

  responseGoogle(response){
    this.props.signInSuccess(response);
  }

  responseGoogleError(response){
    // todo error response
  }

  render() {
    return (
      <div className="login-wrap">
        <Segment className="google-sign-in" padded='very' color='blue'>
          <GoogleLogin
            className="google-login"
            clientId={CLIENT_ID}
            onSuccess={this.responseGoogle}
            onFailure={this.responseGoogleError}>
            <span> Login with Google</span>
          </GoogleLogin>
        </Segment>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
    authenticated: isAuthenticated(state)
});

export default connect(mapStateToProps, {signInSuccess})(Login);
