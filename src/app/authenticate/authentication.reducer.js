import * as constant from './autentication.constants';

export default function (state = {}, action) {

  switch (action.type) {
    case constant.AUTH_USER:
      return {
        ...state,
        authenticated: true,
        user: action.payload.profileObj
      };

    case constant.UNAUTH_USER:
      return {
        ...state,
        authenticated: false,
        user: null
      };

    default:
      return state;
  }
};

// Selectors

export const _getUser = (state) => state.user;
export const _isAuthenticated = (state) => state.authenticated;