import * as constant from './autentication.constants';
import * as localStorageService from '../localstorage/localstorage-service';

// sign in with google
// save login token
// redirect user to correct route
// because we are using a thunk middleware, we are going ot return function
// from the action creator

export const signInSuccess = (token) => {
    return function(dispatch){
        dispatch(
            {
                type: constant.AUTH_USER,
                payload: token
            }
        );

        localStorageService.saveTokenToLocalStorage(token);
    }
};

export const signOut = () => {
    return function (dispatch){
        dispatch(
            {
                type: constant.UNAUTH_USER
            }
        );
        localStorageService.removeKeyItemFromLocalStorage('googleL9Token');
    }
};
