import React, {Component} from 'react';
import './../styles/general.css';

class ErrorPage extends Component {
    render(){
        return (
            <div className="page">
                <div className="error-page"></div>
                <div className="error-page-text">:(  &nbsp;&nbsp; 404</div>
            </div>
        )
    }
}
export default ErrorPage;
