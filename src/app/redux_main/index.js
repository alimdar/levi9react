import {combineReducers} from 'redux';
import auth from '../authenticate/authentication.reducer';
import posts from '../posts/posts.reducer';
import modal from '../modal/redux';
import media from '../posts/media/media.reducer';
import {reducer as formReducer} from 'redux-form';

/** State shape
 {
    auth: {
        authenticated: Bool,
        user: Object
    },
    posts: {
        myPosts: Array<Object>,
        cachedValues: Object,
        singlePost: Object
    } ,
    modal: {
        modal: Bool,
        modalSettings: ?
    } ,
    media: {
        savedMedia: Array<Object>,
        singleMedia: Object,
        searchedMedia: Array<Object>,
        mediaForPost: Object
    },
    form: Object
 }
 */


const rootReducer = combineReducers({
  auth,
  posts,
  modal,
  media,
  form: formReducer
});

export default rootReducer;