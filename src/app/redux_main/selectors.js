import {_getUser, _isAuthenticated} from '../authenticate/authentication.reducer';
import {modalSelectors} from '../modal/redux/index';
import {_getMediaForPost, _getSavedMedia, _searchedMedia} from '../posts/media/media.reducer';
import {_getCachedValues, _getMyPosts, _singlePost} from '../posts/posts.reducer';


/** Public selectors **/

// Auth
export const getUser = (state) => _getUser(state.auth);
export const isAuthenticated = (state) => _isAuthenticated(state.auth);

// Modal
export const getModal = (state) => modalSelectors.getModal(state.modal);
export const getModalSettings = (state) => modalSelectors.getModalSettings(state.modal);

// Media
export const getSavedMedia = (state) => _getSavedMedia(state.media);
export const searchedMedia = (state) => _searchedMedia(state.media);
export const getMediaForPost = (state) => _getMediaForPost(state.media);

// Post
export const getCachedValues = (state) => _getCachedValues(state.posts);
export const singlePost = (state) => _singlePost(state.posts);
export const getMyPosts = (state) => _getMyPosts(state.posts);

// Form
export const getFormValues = (state, formName) => state.form[formName] ? state.form[formName].values : false;