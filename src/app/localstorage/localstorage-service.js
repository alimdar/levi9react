// AUTHENTICATION
export function saveTokenToLocalStorage(token) {
  const tokenJson = JSON.stringify(token);
  localStorage.setItem('googleL9Token', tokenJson);
}

// GENERAL
export function removeKeyItemFromLocalStorage(itemKey) {
  const item = localStorage.getItem(itemKey);
  if (item) {
    localStorage.removeItem(itemKey);
  }
}

export function getItemFromLocalStorage(itemKey) {
  // Using try/catch to avoid error if user privacy mode disables local storage.
  try {
    let serializedItem = localStorage.getItem(itemKey);
    if (!serializedItem) {
      return undefined;
    }
    return JSON.parse(serializedItem);
  } catch(err){
    console.log(`Error retrieving ${itemKey} from localStorage`, err);
    return undefined;
  }
}


export function loadState(){
  return getItemFromLocalStorage('stateL9');
}

export function saveState(state){
  try {
    let serializedState = JSON.stringify(state);
    localStorage.setItem('stateL9', serializedState);
  } catch(err){
    console.log(`Error retrieving state from localStorage`, err);
  }

}