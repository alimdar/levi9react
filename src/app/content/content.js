import React, {Component} from 'react';

import {Switch, Route, Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';

import Login from './../../app/authenticate/login/login'
import ErrorPage from "../404page/404page";
import routes from '../../routes';
import {isAuthenticated} from '../redux_main/selectors'

class Content extends Component {

  render() {
    return (
        <Switch>
          {routes.map((route) => (
              <Route exact
                     key={route.path}
                     path={route.path}
                     render={() => (
                         this.props.authenticated ?
                             route.component :
                             <Redirect to={route.redirectTo}/>
                     )}/>
          ))}
          <Route path="/login" render={() => (
              this.props.authenticated ?
                  <Redirect to="/"/> :
                  <Login/>
          )}/>
          <Route component={ErrorPage}/>
        </Switch>
    )
  }
}

const mapStateToProps = (state) => ({
  authenticated: isAuthenticated(state)
});


export default withRouter(connect(mapStateToProps)(Content));