import {applyMiddleware, compose, createStore} from 'redux';
import thunk from 'redux-thunk';
import throttle from 'lodash/throttle';
import reducer from './app/redux_main';
import {loadState, saveState} from './app/localstorage/localstorage-service';

const configureStore = () => {
  // for react chrome debugging
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  // Load state from local storage.
  const stateFromLocalStorage = loadState();

  // create store
  let store = createStore(reducer, stateFromLocalStorage, composeEnhancers(applyMiddleware(thunk)));

  store.subscribe(throttle(() => {
    saveState({
      media: store.getState().media,
      posts: store.getState().posts,
    });
  }), 1000);

  return store;
};

export default configureStore;
