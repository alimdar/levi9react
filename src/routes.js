import React from 'react';

import PostsList from './app/posts/posts-list/posts-list';
import MediaSearch from './app/posts/media/media-search/media-search';
import PostPage from './app/posts/post-page/post-page';
import NewPost from "./app/posts/new-post/new-post";
import MediaSaved from "./app/posts/media/media-saved/media-saved";

export default [
  {
    path: '/',
    component: <PostsList/>,
    redirectTo: '/login'
  },
  {
    path: '/post/:postId',
    component: <PostPage/>,
    redirectTo: '/login'
  },
  {
    path: '/newpost',
    component: <NewPost/>,
    redirectTo: '/login'
  },
  {
    path: '/search/:platform',
    component: <MediaSearch/>,
    redirectTo: '/login'
  },
  {
    path: '/search/add/:platform',
    component: <MediaSearch/>,
    redirectTo: '/login'
  },
  {
    path: '/mediasaved',
    component: <MediaSaved/>,
    redirectTo: '/login'
  }
];