import React from 'react';
import App from './App';
import {Provider} from 'react-redux';
import {BrowserRouter} from 'react-router-dom';

const Root = ({store}) => (
  <BrowserRouter>
    <Provider store={store}>
      <App/>
    </Provider>
  </BrowserRouter>
);

export default Root;


