import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import 'semantic-ui-css/semantic.min.css';
import './app/styles/index.scss';
import {AUTH_USER} from "./app/authenticate/autentication.constants";
import {getItemFromLocalStorage} from './app/localstorage/localstorage-service';
import configureStore from './configure.store';
import Root from './root';

const store = configureStore();
const token = getItemFromLocalStorage('googleL9Token');

if (token) {
  store.dispatch(
    {
      type: AUTH_USER,
      payload: token
    }
  );
}

ReactDOM.render(
  <Root store={store} />,
  document.getElementById('root')
);

registerServiceWorker();
